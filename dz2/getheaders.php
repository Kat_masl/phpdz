<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Document</title>
</head>

<body style="display: block; background-color: #383838;">
    <header style="display: flex; flex-direction: row; justify-content: space-evenly; margin-top: 20px;">
        <img width="10%" src="https://mospolytech.ru/local/templates/main/dist/img/logos/mospolytech-logo-white.svg" alt="logo">
        <h1 style="align-self: center; display: flex; margin-right: 200px; color: aliceblue; font-size: 32px;">Задание для самостоятельной работы «Feedback form»</h1>
    </header>
    
    <main style="margin-top: 20px; text-align: center; color: aliceblue;">
        <?php
            $url = "https://httpbin.org/post";
        ?>
        <textarea name="" id="" cols="50" rows="20">
            <?php
                print_r(get_headers($url));
            ?>
        </textarea>
    </main>

    <footer style="margin-top: 25px; text-align: center;">
        <p style="color: aliceblue;">Сверстать форму обратной связи. Отправку формы осуществить на URL. Добавить кнопку для перехода на 2 страницу.</p>
    </footer>
</body>
</html>
