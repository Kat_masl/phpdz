<?php
    $database = mysqli_connect('std-mysql.ist.mospolytech.ru', 'std_1715_db', 'wjF1gXRhFp6z', 'std_1715_db');
    if (mysqli_connect_error()) echo '<div class="message error">Ошибка подключения к базе данных..</div>';

    if (isset($_POST['submit']) && $_POST['submit'] == "Обновить запись") {
        $params = array('name', 'lastname', 'gender', 'date', 'phone', 'location', 'email', 'comment');
        
        $query = "UPDATE `notebook` SET `surname` = '" . $_POST['surname'] . "'";
        foreach ($params as $param) {
            $query .= ', `' . $param . "` = '" . $_POST[$param] . "'";
        }
        $query .= ' WHERE `id` = ' . $_GET['id'];

        $result = mysqli_query($database, $query);
        if (mysqli_errno($database)) echo '<div class="message error">Ошибка запроса</div>';
        else echo '<div class="message success">Запись обновлена</div>';
    }

    $query = 'SELECT `id`, `surname`, LEFT(`name`, 1) AS `name`, LEFT(`lastname`, 1) AS `lastname` FROM `notebook`';
    $result = mysqli_query($database, $query);
    if (mysqli_errno($database)) echo '<div class="message error">Ошибка запроса</div>';

    if (!mysqli_num_rows($result)) {
        echo '<div class="message success">В книжке отсутствуют записи</div>';
        exit;
    }

    echo '<div class="users box box--flow">';


    while ($current = mysqli_fetch_assoc($result)) {
        echo '<a class="users__item" href="?page=edit&id=' . $current['id'] . '">' . $current['surname'] . ' ' . $current['name'] . '.' . $current['lastname'] . '.' . '</a>'; 
    }

    echo '</div>';

    if (isset($_GET['id'])) {
        $button = "Обновить запись";

        $query = 'SELECT * FROM `notebook` WHERE `id` = ' . $_GET['id'];
        $result = mysqli_query($database, $query);
        $row = mysqli_fetch_assoc($result);

        include('form.php');
    }
?>
