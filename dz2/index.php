<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>form_masl</title>
</head>
<body style="display: block; background-color: #383838;">
    <header style="display: flex; flex-direction: row; justify-content: space-evenly; margin-top: 20px;">
        <img width="10%" src="https://mospolytech.ru/local/templates/main/dist/img/logos/mospolytech-logo-white.svg" alt="logo">
        <h1 style="align-self: center; display: flex; margin-right: 200px; color: aliceblue; font-size: 32px;">Задание для самостоятельной работы «Feedback form»</h1>
    </header>

    <main class="row" style="color: aliceblue;">
        <div class="container col-lg-4">
            <form action="https://httpbin.org/post" method="POST">
                <div class="mb-4">
                    <label for="name_id" class="form-label">Имя пользователя</label>
                    <div class="col-sm-14">
                        <input type="text" name="name" class="form-control" id="name_id" placeholder="Введите свое имя">
                    </div>
                </div>
                <div class="mb-4">
                    <label for="email_id" class="form-label">E-mail пользователя</label>
                    <div class="col-sm-14">
                        <input type="email" name="email" class="form-control" id="email_id" placeholder="Введите свое имя">
                    </div>
                </div>
                <div class="mb-4">
                    <label for="select_id" class="form-label">Тип обращения</label>
                    <div class="col-sm-14">
                        <select id="select_id" name="select" class="form-select">
                            <option value="appeal">Жалоба</option>
                            <option value="offer">Предложение</option>
                            <option value="thanks">Благодарность</option>
                        </select>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="appeal_id" class="form-label">Текст обращения</label>
                    <textarea class="form-control" name="appeal" id="appeal_id" rows="3"></textarea>
                </div>
                <fieldset class="row mb-4">
                    <legend class="col-form-label">Вариант ответа</legend>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="SMS" id="SMS" value="SMS" checked>
                            <label class="form-check-label" for="SMS">
                            SMS
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="e-mail" id="e-mail" value="e-mail">
                            <label class="form-check-label" for="e-mail">
                            E-mail
                            </label>
                        </div>
                    </div>
                </fieldset>
                <div>
                    <button type="submit" class="btn btn-primary">Отправить</button>
                </div>
                <a href="getheaders.php">Ссылка на 2 страницу</a>
            </form>
        </div>
    </main>

    <footer style="margin-top: 25px; text-align: center;">
        <p style="color: aliceblue;">Сверстать форму обратной связи. Отправку формы осуществить на URL. Добавить кнопку для перехода на 2 страницу.</p>
    </footer>
</body>
</html>
