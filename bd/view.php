<?php

    function getFriendList() {
        $database = mysqli_connect('std-mysql.ist.mospolytech.ru', 'std_1715_db', 'wjF1gXRhFp6z', 'std_1715_db');
        if (mysqli_connect_error()) echo '<div class="message error">Ошибка подключения к базе данных..</div>';

        $query = 'SELECT * FROM `notebook`';
        $result = mysqli_query($database, $query);

        if (!mysqli_num_rows($result)) {
            return '<div class="message success">В книжке отсутствуют записи</div>';
        }

        $count_pages = floor(mysqli_num_rows($result) / 10);

        if (!isset($_GET['current-page'])) $_GET['current-page'] = 0;

        $query = 'SELECT * FROM `notebook`';
        if ($_GET['sort'] == 'byname') $query .= 'ORDER BY `surname`';
        $query .= ' LIMIT ' . $_GET['current-page'] * 10 . ', 10';
        $result = mysqli_query($database, $query);

        $titles = array('ID', 'Фамилия', 'Имя', 'Отчество', 'Пол', 'Дата Рождения', 'Телефон', 'Адрес', 'Email', 'Комментарий');

        $html = '<div class="box box--flow"><table class="table"><thead class="table__header"><tr>';
        
        foreach ($titles as $title) $html .= '<th class="table__title">' . $title . '</th>';
        
        $html .= '</tr></thead><tbody class="table__content">';
        
        while ($current = mysqli_fetch_assoc($result)) {
            $html .= '<tr class="table__item">';

            foreach ($current as $item) {
                $html .= '<td class="table__property">' . $item . '</td>';
            }

            $html .= '</tr>';
        }

        $html .= '</tbody></table>';

        if ($count_pages) {
            $html .= '<div class="pagination">';
        
            for ($i = 0; $i <= $count_pages; $i++) {
                $html .= '<a class="pagination__link"';
                
                if ($_GET['current-page'] != $i) $html .= 'href="?page=view&current-page=' . $i . '"';

                $html .= '>' . ($i + 1) . '</a>';
            }

            $html .= '</div>';
        }
        
        $html .= "</div>";

        return $html;
    }

?>
