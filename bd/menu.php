<?php
    echo '<header class="header"><div class="header__wrapper box">';

    $menu = array(
        'view' => "Просмотр",
        'add' => "Добавление записи",
        'edit' => "Редактирование записи",
        'delete' => "Удаление записи"
    );

    if (!isset($_GET['page'])) $_GET['page'] = 'view';

    foreach ($menu as $menu_link => $menu_title) {
        echo '<a class="header__link ';
        if ($_GET['page'] == $menu_link) echo 'select';
        echo '" href="?page=' . $menu_link . '"';
        
        echo '>' . $menu_title . '</a>';
    }

    echo '</div></header>';

    if ($_GET['page'] == 'view') {
        $sorts = array(
            'byid' => "По умолчанию",
            "byname" => "По фамилии"
        );
    
        echo '<div class="submenu box box--flow">';

        if (!isset($_GET['sort'])) $_GET['sort'] = 'byid';

        foreach ($sorts as $sort_link => $sort_title) {
            echo '<a class="submenu__link ';
            if ($_GET['sort'] == $sort_link) echo 'select';
            echo '" href="?page=view&sort=' . $sort_link . '">' . $sort_title . '</a>';
        }
    }

    echo '</div>';
?>
