<?php if (!isset($row)) $row = array(
    "surname" => "",
    "name" => "",
    "lastname" => "",
    "sex" => "Мужской",
); ?>

<div class="box box--flow">
    <form class="form" name="form_add" method="post">
        <div class="form__field">
            <label for="surname" class="form__label">Фамилия</label>
            <input type="text" name="surname" id="surname" class="form__input" value="<?=$row['surname']?>">
        </div>
        <div class="form__field">
            <label for="name" class="form__label">Имя</label>
            <input type="text" name="name" id="name" class="form__input" value="<?=$row['name']?>">
        </div>
        <div class="form__field">
            <label for="lastname" class="form__label">Отчество</label>
            <input type="text" name="lastname" id="lastname" class="form__input" value="<?=$row['lastname']?>">
        </div>
        <div class="form__field">
            <label for="gender" class="form__label">Пол</label>
            <select name="gender" id="gender" class="form__input form__input--select">
                <option value="Мужской">Мужской</option>
                <option value="Женский">Женский</option>
            </select>
        </div>
        <div class="form__field">
            <label for="date" class="form__label">Дата Рождения</label>
            <input type="date" name="date" id="date" class="form__input" value="<?=$row['date']?>">
        </div>
        <div class="form__field">
            <label for="phone" class="form__label">Телефон</label>
            <input type="text" name="phone" id="phone" class="form__input" value="<?=$row['phone']?>">
        </div>
        <div class="form__field">
            <label for="location" class="form__label">Адрес</label>
            <input type="text" name="location" id="location" class="form__input" value="<?=$row['location']?>">
        </div>
        <div class="form__field">
            <label for="email" class="form__label">Email</label>
            <input type="email" name="email" id="email" class="form__input" value="<?=$row['email']?>">
        </div>
        <div class="form__field">
            <label for="email" class="form__label">Комментарий</label>
            <textarea name="comment" placeholder="Краткий комментарий" id="comment" class="form__input form__input--textarea"><?=$row['comment']?></textarea>
        </div>
        <button type="submit" value="<?=$button?>" name="submit" class="form__button"><?=$button?></button>
    </form>
</div>
