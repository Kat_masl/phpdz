<?php
    $database = mysqli_connect('std-mysql.ist.mospolytech.ru', 'std_1715_db', 'wjF1gXRhFp6z', 'std_1715_db');
    if (mysqli_connect_error()) echo '<div class="message error">Ошибка при подключении</div>';

    if (isset($_GET['id'])) {
        $query = "DELETE FROM `notebook` WHERE `id` = " . $_GET['id'];
        $result = mysqli_query($database, $query);
        if (mysqli_errno($database)) echo '<div class="message error">Ошибка запроса</div>';
        else echo '<div class="message success">Запись удалена</div>';
    }

    $query = 'SELECT `id`, `surname`, LEFT(`name`, 1) AS `name`, LEFT(`lastname`, 1) AS `lastname` FROM `notebook`';
    $result = mysqli_query($database, $query);
    if (mysqli_errno($database)) echo '<div class="message error">Ошибка запроса</div>';

    if (!mysqli_num_rows($result)) {
        echo '<div class="message success">Записи отсутствуют</div>';
        exit;
    }

    echo '<div class="users box box--flow">';


    while ($current = mysqli_fetch_assoc($result)) {
        echo '<a class="users__item" href="?page=delete&id=' . $current['id'] . '">' . $current['surname'] . ' ' . $current['name'] . '.' . $current['lastname'] . '.' . '</a>'; 
    }

    echo '</div>';
?>
